import React, { useState } from 'react';
import { Button, Grid, TextField } from '@material-ui/core';

const Calculator = () => {
  const [total, setTotal] = useState(0);
  const [operation, setOperation] = useState(null);
  const [newData, setNewData] = useState(false);
  const [inputVal, setInputVal] = useState(0);
  const addNumber = (number) => {
    if (newData){
      setInputVal(""+number);
      setNewData(false);
    } else {
      setInputVal(""+inputVal+number);
    }
  };

  const makeOperation = (operador, num1, num2) => {
    let res;
    switch (operador) {
      case 'add':
        res = parseFloat(num1) + parseFloat(num2);
        break;
      case 'subtract':
        res = parseFloat(num1) - parseFloat(num2);
        break;
      case 'multiply':
        res = parseFloat(num1) * parseFloat(num2);
        break;
      case 'divide':
        res = parseFloat(num1) / parseFloat(num2);
        break;
      default:
        break;
    }
    return res;
  };

  const handleOperator = (operator) => {
    if ( operator === 'ac') {
        setInputVal(0);
        setTotal(0);
        setNewData(true);
        setOperation(null);
        return;
    }
    
    if (operation) {
      let auxTotal = makeOperation(operation, total, inputVal);
      
      switch (operator) {
        case 'equal':
          setInputVal(auxTotal);
          setTotal(auxTotal);
          setNewData(true);
          setOperation(null);
          break;
        default:
          setTotal(auxTotal);
          setInputVal(auxTotal);
          setOperation(operator);
          setNewData(true);
          break;
      }
      
    } else {
      setOperation(operator);
      setTotal(inputVal);
      setNewData(true);
    }
  };
  return (
    <>
      <Grid container style={{ paddingTop: '10px'}}>
        <Grid item xs={12}>
        <TextField
          id="outlined-number"
          label="Calculadora"
          type="number"
          InputLabelProps={{
            shrink: true,
          }}
          variant="outlined"
          fullWidth
          value={inputVal}
          inputProps={{style: { textAlign: 'right' }}}
        />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(1)} fullWidth variant="contained" >1</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(2)} fullWidth variant="contained" >2</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(3)} fullWidth variant="contained" >3</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => handleOperator('add')} fullWidth variant="contained" >+</Button>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(4)} fullWidth variant="contained" >4</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(5)} fullWidth variant="contained" >5</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(6)} fullWidth variant="contained" >6</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => handleOperator('subtract')} fullWidth variant="contained" >-</Button>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(7)} fullWidth variant="contained" >7</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(8)} fullWidth variant="contained" >8</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(9)} fullWidth variant="contained" >9</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => handleOperator('multiply')} fullWidth variant="contained" >*</Button>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={3}>
          <Button onClick={() => addNumber(0)} fullWidth variant="contained" >0</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => handleOperator('ac')} fullWidth variant="contained" >AC</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => handleOperator('equal')} fullWidth variant="contained" >=</Button>
        </Grid>
        <Grid item xs={3}>
          <Button onClick={() => handleOperator('divide')} fullWidth variant="contained"  >/</Button>
        </Grid>
      </Grid>
    </>
  );
};

export default Calculator;