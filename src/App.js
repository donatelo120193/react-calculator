import { Container } from '@material-ui/core';
import Calculator from './components/Calculator';

function App() {
  return (
    <Container maxWidth="sm">
        <Calculator />
    </Container>
  );
}

export default App;
